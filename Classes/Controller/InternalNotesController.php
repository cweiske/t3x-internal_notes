<?php

namespace CodingMs\InternalNotes\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Thomas Deuling <typo3@coding.ms>, coding.ms
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;
use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;
use TYPO3\CMS\Backend\View\BackendTemplateView;
use TYPO3\CMS\Backend\Template\Components\ButtonBar;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Imaging\IconFactory;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use \TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/**
 * Internal notes controller
 */
class InternalNotesController extends ActionController
{

    /**
     * Object-Manager
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     * @inject
     */
    protected $objectManager;

    /**
     * @var \CodingMs\Modules\Utility\BackendListUtility
     * @inject
     */
    protected $backendListUtility = null;

    /**
     * Internal note Repository
     *
     * @var \CodingMs\InternalNotes\Domain\Repository\InternalNoteRepository
     * @inject
     */
    protected $internalNoteRepository;

    /**
     * BackendTemplateContainer
     *
     * @var BackendTemplateView
     */
    protected $view;

    /**
     * Backend Template Container
     *
     * @var BackendTemplateView
     */
    protected $defaultViewObjectName = BackendTemplateView::class;

    /**
     * @var IconFactory
     */
    protected $iconFactory;

    /**
     * @var int
     */
    protected $pageUid;

    /**
     * Set up the doc header properly here
     *
     * @param ViewInterface $view
     */
    protected function initializeView(ViewInterface $view)
    {
        $this->pageUid = (int)GeneralUtility::_GP('id');
        /** @var BackendTemplateView $view */
        parent::initializeView($view);
        if ($this->view->getModuleTemplate() !== null) {
            $pageRenderer = $this->view->getModuleTemplate()->getPageRenderer();
            $extRealPath = '../' . ExtensionManagementUtility::siteRelPath('modules');
            $pageRenderer->addCssFile($extRealPath . 'Resources/Public/Stylesheets/Modules.css');
            // Initialize icon factory
            $this->iconFactory = GeneralUtility::makeInstance(IconFactory::class);
            // Create menu and buttons
            $this->createMenu();
            $this->createButtons();
        }
    }

    /**
     * Create action menu
     *
     * @return void
     */
    protected function createMenu()
    {
        /** @var UriBuilder $uriBuilder */
        $uriBuilder = $this->objectManager->get(UriBuilder::class);
        $uriBuilder->setRequest($this->request);
        $menu = $this->view->getModuleTemplate()->getDocHeaderComponent()->getMenuRegistry()->makeMenu();
        $menu->setIdentifier('internal_notes');
        $actions = [
            [
                'action' => 'list',
                'label' => LocalizationUtility::translate('tx_internalnotes_label.list', $this->extensionName)
            ]
        ];
        foreach ($actions as $action) {
            $item = $menu->makeMenuItem()
                ->setTitle($action['label'])
                ->setHref($uriBuilder->reset()->uriFor($action['action'], [], 'InternalNotes'))
                ->setActive($this->request->getControllerActionName() === $action['action']);
            $menu->addMenuItem($item);
        }
        $this->view->getModuleTemplate()->getDocHeaderComponent()->getMenuRegistry()->addMenu($menu);
    }

    /**
     * Add menu buttons for specific actions
     *
     * @return void
     */
    protected function createButtons()
    {
        $buttonBar = $this->view->getModuleTemplate()->getDocHeaderComponent()->getButtonBar();
        $uriBuilder = $this->objectManager->get(UriBuilder::class);
        $uriBuilder->setRequest($this->request);
        $buttons = [];
        switch ($this->request->getControllerActionName()) {
            case 'list': {
                // New note
                $parameter = [
                    'id' => $this->pageUid,
                    'tx_internalnotes_web_internalnotesinternalnotes' => [
                        'action' => 'list',
                        'controller' => 'InternalNotes'
                    ]
                ];
                $returnUrl = BackendUtility::getModuleUrl('web_InternalNotesInternalnotes', $parameter);
                $parameter = [
                    'returnUrl' => $returnUrl,
                    'edit' => [
                        'sys_note' => [
                            $this->pageUid => 'new'
                        ]
                    ]
                ];
                $title = LocalizationUtility::translate('tx_internalnotes_label.new_note', $this->extensionName);
                $buttons[] = $buttonBar->makeLinkButton()
                    ->setHref(BackendUtility::getModuleUrl('record_edit', $parameter))
                    ->setTitle($title)
                    ->setIcon($this->iconFactory->getIcon('actions-document-new', Icon::SIZE_SMALL));
                // CSV export
                /** @todo implement CSV export
                $parameter = [
                    'csv' => 1
                ];
                $title = LocalizationUtility::translate('tx_internalnotes_label.csv_export', $this->extensionName);
                $buttons[] = $buttonBar->makeLinkButton()
                    ->setHref(BackendUtility::getModuleUrl('web_InternalNotesInternalnotes', $parameter))
                    ->setTitle($title)
                    ->setIcon($this->iconFactory->getIcon('actions-document-export-csv', Icon::SIZE_SMALL));
                break;*/
            }
        }
        foreach ($buttons as $button) {
            $buttonBar->addButton($button, ButtonBar::BUTTON_POSITION_LEFT);
        }
    }

    /**
     * List for internal notes (sys_notes)
     *
     * @return void
     */
    public function listAction()
    {
        // Build list
        $list = $this->backendListUtility->initList($this->settings['lists']['internalNotes'], $this->request);
        // Get categories
        global $TCA;
        $list['category'] = [
            'items' => [],
            'icons' => [],
            'selected' => -1
        ];
        foreach ($TCA['sys_note']['columns']['category']['config']['items'] as $categoryKey => $category) {
            if($category[1] == 0) {
                $list['category']['items'][$category[1]] = 'Notice';
            }
            else {
                $list['category']['items'][$category[1]] = $GLOBALS['LANG']->sL($category[0]);
            }
            $list['category']['icons'][$category[1]] = $category[2];
        }
        // Selected category
        if($this->request->hasArgument('category')) {
            $list['category']['selected'] = (int)$this->request->getArgument('category');
        }
        // Global notices
        $list['global'] = false;
        if($this->request->hasArgument('global')) {
            $list['global'] = true;
        }
        // Store settings
        $this->backendListUtility->writeSettings($list['id'], $list);
        // Export Result as CSV!?
        if ($this->request->hasArgument('csv')) {
            // Get download accesses
            $list['limit'] = 0;
            $list['offset'] = 0;
            $list['pid'] = $this->pageUid;
            $internalNotes = $this->internalNoteRepository->findAllForBackendList($list);
            $list['countAll'] = $this->internalNoteRepository->findAllForBackendList($list, true);
            $this->backendListUtility->exportAsCsv($internalNotes, $list);
        } else {
            // Get download accesses
            $list['pid'] = $this->pageUid;
            $internalNotes = $this->internalNoteRepository->findAllForBackendList($list);
            $list['countAll'] = $this->internalNoteRepository->findAllForBackendList($list, true);
        }
        // Download collections for filtering
        $filter = array();
        $filter['limit'] = 0;
        $filter['offset'] = 0;
        $filter['pid'] = $this->pageUid;
        $this->view->assign('categories', $list['category']['items']);
        $this->view->assign('list', $list);
        $this->view->assign('internalNotes', $internalNotes);
        $this->view->assign('currentPage', $this->pageUid);
        $this->view->assign('actionMethodName', $this->actionMethodName);
    }

}
