<?php
namespace CodingMs\InternalNotes\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 Thomas Deuling <typo3@coding.ms>, coding.ms
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Internal note (sys_note)
 *
 * @package internal_notes
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class InternalNote extends \TYPO3\CMS\SysNote\Domain\Model\SysNote {

	/**
	 * Export
	 * @param array $list
	 * @return array
	 * @throws \Exception
	 */
	public function toCsvArray(array $list=array()) {
		$array = array();
		foreach($list['fields'] as $key=>$options) {
			// Try to catch non existing class methods
			try {
				if($options['hideInExport']==1) {
					continue;
				}
				$array[$key] = '';
				$keyParts = explode('.', $key);
				if(count($keyParts)==2) {
					$firstMethod = $this->checkMethod($this, $keyParts[0]);
					$object = $this->$firstMethod();
					// Is a lazy object?!
					if($object instanceof \TYPO3\CMS\Extbase\Persistence\Generic\LazyLoadingProxy) {
						$object = $object->_loadRealInstance();
					}
					$secondMethod = $this->checkMethod($object, $keyParts[1]);
					$array[$key] = $this->$firstMethod()->$secondMethod();
				}
				else if(count($keyParts)==1) {
					$firstMethod = $this->checkMethod($this, $keyParts[0]);
					$array[$key] = $this->$firstMethod();
				}
				// Format value
				if($options['format'] == 'DateTime') {
					if($array[$key] instanceof \DateTime) {
						$array[$key] = $array[$key]->format($options['dateFormat']);
					}
				}
				else {
					$array[$key] = strip_tags($array[$key]);
				}
			}
				// Non existing class methods found?!
			catch(\Exception $e) {
				// Write the exception message into the cell
				$message = 'Error in settings.' . $list['id'] . '.fields.' . str_replace('.', '_', $key) . "\n";
				$array[$key] = $message . $e->getMessage();
			}
		}
		return $array;
	}

	/**
	 * Export-Helper
	 * @param $object
	 * @param $method
	 * @return string
	 * @throws \Exception
	 */
	protected function checkMethod($object, $method) {
		$checkMethod = 'get' .ucfirst($method);
		if(method_exists($object, $checkMethod)) {
			return $checkMethod;
		}
		else {
			throw new \Exception($checkMethod . ' not found on ' . get_class($this) . '!');
		}
	}

}
