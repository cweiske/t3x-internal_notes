<?php
$EM_CONF[$_EXTKEY] = array(
    'title' => 'Internal notes manager',
    'description' => 'Internal notes manager for sys_notes',
    'category' => 'be',
    'author' => 'Thomas Deuling',
    'author_email' => 'typo3@coding.ms',
    'state' => 'beta',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.1.DEV',
    'constraints' => array(
        'depends' => array(
            'typo3' => '8.7.0-8.7.99',
            'sys_note' => '8.7.0-8.7.99',
            'modules' => '2.0.0-2.99.99',
        ),
        'conflicts' => array(),
        'suggests' => array(),
    ),
);
