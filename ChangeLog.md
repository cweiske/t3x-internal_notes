# Internal-Notes Change-Log



## Release of version 1.0.1

### 2017-08-12

*	[BUGFIX] Fixing requirement issue to modules



## Release of version 1.0.0

### 2017-08-12

*	[TASK] Preparing first release

### 2017-06-29

*	[BUGFIX] Fixing return url for new record view

### 2017-06-28

*	[TASK] Change order of Backend-Module subviews




