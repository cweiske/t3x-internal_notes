*****************************************************
internal_notes - Internal notes manager for sys_notes
*****************************************************

TYPO3 extension that provides a backend module to manage ``sys_note`` notice
records.

Features
========

- List notes of current page
- List notes of all pages ("Global")
- Show personal notes only
- Filter by note type

Screenshot
==========

.. image:: Documentation/screenshot.png
   :alt: Screenshot of internal notes backend module


Links
=====
Source code repository
  https://bitbucket.org/codingms/internal_notes/
TYPO3 extension repository
  https://extensions.typo3.org/extension/internal_notes/
